/*

  Basic.pde - example using ModbusMaster library

  Library:: ModbusMaster
  Author:: Doc Walker <4-20ma@wvfans.net>

  Copyright:: 2009-2016 Doc Walker

*/

#include <ModbusMaster.h>

const int ledPin = LED_BUILTIN ;

// instantiate ModbusMaster object
ModbusMaster node,node2;

long ts;
int incrementa = 1;
  uint16_t vv[13]={0,0,0,0,0,0,0,0,0,0,0,0,0};


void setup()
{
  Serial.begin(19200);
  node.begin(5, Serial);
  node2.begin(6, Serial);
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);
    ts = millis();
    mudaValores();
}

  uint8_t result;

void loop()
{
   if (millis() > ts + 4000) {
       ts = millis();

          if (random(3)==1)
          setArray(node2);
          else
          {
            incrementa++; if(incrementa>6000)incrementa=0;
            mudaValores();
            setArray(node);
          }
   }
}


int mudaValores(){
    vv[1]=1; // Altura
    vv[2]=9; // Capacidade
    for(int v=3;v<7;v++) {vv[v]=random(1, 65535 );} // Altera os numeros
    vv[7]=255;
    vv[8]=incrementa;
    vv[9]=random(1, 65535 );
    vv[10]=random(1, 65535 );
    vv[11]=random(1, 65535 );
}


int setArray(ModbusMaster &node_func){

  for(int v=1;v<13;v++) 
    node_func.setTransmitBuffer(v, vv[v]);

  result = node_func.writeMultipleRegisters(0,13);

      if (result == node_func.ku8MBSuccess)
        pisca(3);
        else
          digitalWrite(ledPin,HIGH);
}


int pisca(int numero){
  for(int v=0;v<numero;v++) 
  {
          digitalWrite(ledPin,HIGH);delay(150);
          digitalWrite(ledPin,LOW);delay(150);
  }
}
