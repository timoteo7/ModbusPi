"use strict";
/* 

MODBUS to HTTP tunnel

Author: Timóteo Apolinário Mello
E-mail: timoteoapolinario@gmail.com
Date: 2018-07-13

*/

const LIB_MODBUS_DEBUG_MODE = true
const lib_modbus = require('modbus').create(LIB_MODBUS_DEBUG_MODE)
const request = require('request')

//const SERVER_URL = "https://stucki.azurewebsites.net/Choque/TesteMartelo"
const SERVER_URL = "http://localhost:3000/testeMartelo"

const SERVER_METHOD = "post"
const POST_VAR_PREFIX = "address_"
const POST_VAR_INITIAL_INDEX = 4001

const MODBUS_SLAVE_DEVICE_NUMBER = 5
const MODBUS_INITIAL_ADDRESS = 0
const MODBUS_ADDRESS_COUNT = 13
const MODBUS_DATA_AVAILABLE_ADDRESS = 7
    //const MODBUS_DATA_AVAILABLE_FLAG = 255
const MODBUS_DATA_SERIAL_ADDRESS = 10


const WATCHDOG_TIMEOUT_MS = 2 * 60 * 1000

const COMM_PREFIX = "USB" //ACM
const COMM_BAUD_RATE = 19200 // 115200  //38400 // 19200

var log = (LIB_MODBUS_DEBUG_MODE) ? console.log : function() {};

function create_slave(device) {
    log(`\n\nCriando slave na porta ${device}`)
    modbus_context = lib_modbus.createSlave({
        con: lib_modbus.createConRtu(MODBUS_SLAVE_DEVICE_NUMBER, device, COMM_BAUD_RATE),
        data: device_memory_map,
        onQuery: query,
        onDestroy: function() { console.log('onDestroy'); },
        onConnect: function() { console.log('onConnect'); },
    })
}

function query() {
    log("Query event, ModBus consultado")
    clearTimeout(timeout_handler)
    watchdog()

    var values = modbus_context.getRegs(MODBUS_INITIAL_ADDRESS, MODBUS_ADDRESS_COUNT)
    log(`Values read: ${values} \n\n`)
    modbus_context.setRegs(MODBUS_INITIAL_ADDRESS, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

   if (/*(values[MODBUS_DATA_AVAILABLE_ADDRESS] != 0) &&*/
        ((values[MODBUS_DATA_SERIAL_ADDRESS] != 0) ||
            (values[MODBUS_DATA_SERIAL_ADDRESS + 1] != 0))) {
        log(`Posting values...`)
        setImmediate(() => {
            post_values(values)
        })
    }
}

function post_values(values) {
    log('post_values : ' + values)
    var post_data = {}

    for (var i = 0; i < values.length; i++) {
        post_data[`${POST_VAR_PREFIX}` + (POST_VAR_INITIAL_INDEX + i)] = values[i];
    }

    var options = {
        method: SERVER_METHOD,
        url: SERVER_URL,
        form: post_data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    log(`\nSending to server: ${JSON.stringify(options)}`)
    request(options, (err, res, returned_body) => {
        if (err) {
            log(`POST error: ${err}`)
            return
        }
        log(`POST OK: ${returned_body}`)
    })
}

function watchdog() {
    timeout_handler = setTimeout(() => {
            console.log(`${new Date()} WatchDog exiting after ${WATCHDOG_TIMEOUT_MS} ms.`)
            
            return process.abort()
        },
        WATCHDOG_TIMEOUT_MS)
}

lib_modbus.onError((message) => {
    log(`onError event, message: ${message}`)
    if (COMM_ADDRESS++ > 20) COMM_ADDRESS = 0
    log(`Try to create slave again on /dev/tty${COMM_PREFIX}${COMM_ADDRESS}`)
    create_slave(`/dev/tty${COMM_PREFIX}${COMM_ADDRESS}`)
})

var COMM_ADDRESS = 0
var modbus_context
var device_memory_map = lib_modbus.createData({ countReg: MODBUS_ADDRESS_COUNT + 2 })
var timeout_handler

watchdog()

create_slave(`/dev/tty${COMM_PREFIX}${COMM_ADDRESS}`)
