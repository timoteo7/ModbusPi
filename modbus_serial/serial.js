/* eslint-disable node/no-missing-require */
'use strict';

const SerialPort = require('serialport');
const port = new SerialPort('/dev/ttyUSB0', {
  baudRate: 19200 // 115200  //38400 // 19200
});

port.on('open', () => console.log('Port open'));

port.on('data', (data) => {
  console.log('Received:\t', data);
});

port.on('close', function() { 
  console.log('Desconectou');
 });

//port.write('ROBOT PLEASE RESPOND\n');

// The parser will emit any string response
