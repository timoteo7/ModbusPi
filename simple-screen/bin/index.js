#!/usr/bin/env node

const url = "http://localhost:3000/status";
const sleep = 5 * 1000;

const chalk = require("chalk");
const boxen = require("boxen");
var request = require("request");

// console color
// console.log("\x1b[34mnnamdi\x1b[89m")

function show() {
    setTimeout(() => {
        //clear command
        console.log("\033[2J")
        request(url, function(error, response, body) {
            var greeting = {}

            if (error || !response) {
                greeting = chalk.red.bold("Erro interno: serviço não iniciado")
            } else if (response.statusCode === 200) {
                var info = JSON.parse(body)
                internet = (info.internet ? chalk.green : chalk.red)
                sync = (info.sync ? chalk.green : chalk.red)
                receive = (info.receive ? chalk.green : chalk.red)

                greeting = chalk.bgWhiteBright.black.bold(
                    "INTERNET: " + internet.bold("⬤\n\n") +
                    "SINCRONISMO: " + sync.bold("⬤\n\n") +
                    "RECEBIMENTO: " + receive.bold("⬤\n\n") +
                    "DADOS LOCAIS: " + chalk.red(info.length + "\n\n") +
                    "ESPAÇO EM DISCO: " + chalk.red(info.disk + "%"));
            }

            const boxenOptions = {
                padding: 5,
                borderStyle: "round",
                borderColor: "white",
                dimBorder: true,
                backgroundColor: "#EEE",
                float: "center"
            };
            const msgBox = boxen(greeting, boxenOptions);


            console.log(msgBox);
            show();
        });
    }, sleep);
}

show();
