'use strict';

module.exports = function(app) {
    var controller = require('../controllers/controller');

    // todoList Routes
    app.route('/status')
        .get(controller.getStatus)

    app.route('/testeMartelo')
        .post(controller.testInsert);

};