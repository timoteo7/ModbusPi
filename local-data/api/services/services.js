'use strict';

// settings
const SERVER_URL = "http://stucki.azurewebsites.net/api/"
const CITY = "saoluiz"
const STATUS_TIME = 60 * 1000
const SENDING_TIME = 40 * 1000
const QUEUE_NAME = "tests"

const axios = require('axios').default;
const checkDiskSpace = require('check-disk-space')
const RedisSMQ = require("rsmq");
const rsmq = new RedisSMQ({ host: "127.0.0.1", port: 6379, ns: "rsmq" });

var timeout_handler
var status = {
    internet: false,
    sync: false,
    receive: false,
    length: 0,
    disk: 0,
    modified: 0,
    created: 0,
}

// initialize
// - queue

rsmq.createQueue({ qname: QUEUE_NAME }, function(err, resp) {
    if (resp === 1) {
        console.log(`queue '${QUEUE_NAME}' created`)
    } else console.log(`queue '${QUEUE_NAME}' already exists`)
});

keepUpdatedStatus() // keep updated
keepSendingData() // send data

async function keepUpdatedStatus() {
    updateStatus()
    setTimeout(async() => {
        keepUpdatedStatus()
    }, STATUS_TIME);
}

function updateStatus() {
    // set status.internet
    let refresh = new Date().getTime()
    axios.get(`http://www.google.com?forceRefresh=${refresh}`)
        .then((r) => {
            status.internet = (r.status === 200) //true
        })
        .catch((e) => {
            status.internet = false
        })

    // set status.sync
    axios.get(`${SERVER_URL}/ping/${CITY}?forceRefresh=${refresh}`)
        .then((r) => {
            status.sync = (r.status === 200)
        })
        .catch((e) => {
            status.sync = false
        })
}

async function keepSendingData() {
    if (status.internet && status.sync) {
        sendData()
    }
    setTimeout(async() => {
        keepSendingData()
    }, SENDING_TIME);
}

function sendData() {
    rsmq.receiveMessage({ qname: QUEUE_NAME }, function(err, resp) {
        if (resp.id) {
            var data = JSON.parse(resp.message)
            axios.post(`${SERVER_URL}/TesteMartelo/${CITY}`, data)
                .then(function(response) {
                    if (response.status === 200) {
                        // delete from queue
                        rsmq.deleteMessage({ qname: QUEUE_NAME, id: resp.id }, function(err, deleted) {
                            if (deleted === 1) {
                                console.log("Message deleted.", resp, response.data);
                                sendData()
                            } else {
                                console.log("Message not found.")
                            }
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                    return
                });
        } else {
            return
        }
    });
}

exports.getStatus = function(req, res) {
    checkDiskSpace('/').then((diskSpace) => {
        status.disk = Number(diskSpace.free / diskSpace.size * 100).toFixed(2)
    })

    rsmq.getQueueAttributes({ qname: QUEUE_NAME }, function(err, resp) {
        if (err) {
            console.error(err);
            res.send(err)
        }

        status.created = resp.created;
        status.modified = resp.modified;
        status.length = resp.msgs;

        res.json(status);
    });
    console.log("getStatus", status)
};

exports.testInsert = function(req, res) {
    console.log("Message receive")
    status.receive = true;
    clearTimeout(timeout_handler)
    timeout_handler = setTimeout(() =>  status.receive = false, 1000 * 60);
    rsmq.sendMessage({ qname: QUEUE_NAME, message: JSON.stringify(req.body) }, function(err, resp) {
        if (resp) {
            console.log("Message sent. ID:", resp);
            if (status.sync) sendData()
        }
        let answer = {}
        answer.data = req.body
        answer.resp = resp
        res.json(answer)
    });
};
